# -*- coding: utf-8 -*-

import time
import select
import socket
from typing import Optional

import _helper


class Master:

    def __init__(self, communicate_addr: str, customer_addr: str, debug: bool = False):
        r"""NAT转发master程序

        :param communicate_addr: master程序监听地址，如：0.0.0.0:8000
        :param customer_addr: 客户端转发地址，如：127.0.0.1:3240
        :param debug: DEBUG模式，打印recv数据（from slaver）和send数据（from customer）
        """

        self.DEBUG = debug
        # 初始化socket
        self.__communicate_sock = _helper.create_sock_server(communicate_addr)
        self.__customer_sock = _helper.create_sock_server(customer_addr)
        # slaver列表
        self.__slaver_list = []
        # slaver监听线程
        _helper.start_thread(self.__listen_slaver)
        # customer监听线程
        _helper.start_thread(self.__listen_customer)
        # slaver心跳线程
        _helper.start_thread(self.__heart_beat_daemon)

    def __listen_slaver(self):
        r"""监听新的slaver连接，把新的slaver连接加入列表

        :return:
        """

        while True:
            slaver_conn, slaver_addr = self.__communicate_sock.accept()
            print("新的slaver连接: {}".format(slaver_addr))
            try:
                # 接收初次握手数据："HI"
                if slaver_conn.recv(1024) == bytes("HI", encoding="UTF-8"):
                    # 发送初次握手校验通过信息："OK"
                    slaver_conn.send(bytes("OK", encoding="UTF-8"))
                    # 校验客户端握手通过消息："READY"
                    if slaver_conn.recv(1024) == bytes("READY", encoding="UTF-8"):
                        # 2次握手数据格式校验通过，将slaver存入列表
                        print("新的slaver握手校验通过：{}".format(slaver_addr))
                        self.__slaver_list.append(slaver_conn)
                    else:
                        # 握手校验失败，关闭slaver连接
                        _helper.close_socket(slaver_conn)
            except Exception as e:
                # TODO: 确认可能抛出的异常类型
                print(e)
                _helper.close_socket(slaver_conn)

    def __listen_customer(self):
        r"""监听新的customer连接，为customer分配可用的slaver，并开启1个新的数据交换线程

        :return:
        """

        while True:
            customer_conn, customer_addr = self.__customer_sock.accept()
            print("新的customer连接: {}".format(customer_addr))
            slaver_conn = self.__pop_available_slaver()
            if slaver_conn is not None:
                # 取到了可用的slaver套接字，和当前customer套接字开启新的数据交换线程
                print("开启新线程交换数据")
                _helper.start_thread(self.__tcp_transfer, args=(customer_conn, slaver_conn))
            else:
                print("没有可用的slaver，关闭当前customer")
                _helper.close_socket(customer_conn)

    def __tcp_transfer(self, customer_conn: socket.socket, slaver_conn: socket.socket):
        r"""

        :param customer_conn:
        :param slaver_conn:
        :return:
        """

        while True:
            # 阻塞等待socket满足readable条件
            readable_sockets, _, _ = select.select([customer_conn, slaver_conn], [], [])
            for item in readable_sockets:
                data = b""
                try:
                    # customer发往slaver的数据（向客户端slaver发送的方向）
                    if item == customer_conn:
                        data = customer_conn.recv(1024)
                        if self.DEBUG:
                            print("send:\n{}".format(data))
                        slaver_conn.send(data)
                    # slaver发往customer的数据（从客户端slaver接收的方向）
                    elif item == slaver_conn:
                        data = slaver_conn.recv(1024)
                        if self.DEBUG:
                            print("recv:\n{}".format(data))
                        customer_conn.send(data)
                except ConnectionResetError:
                    print("远程主机强迫关闭了一个现有的连接")
                    _helper.close_socket(customer_conn)
                    _helper.close_socket(slaver_conn)
                    return
                except Exception as e:
                    # TODO: 确认可能抛出的异常类型
                    print(e)
                    _helper.close_socket(customer_conn)
                    _helper.close_socket(slaver_conn)
                    return
                # 没有数据，结束线程
                if not data:
                    _helper.close_socket(customer_conn)
                    _helper.close_socket(slaver_conn)
                    return

    def __heart_beat_daemon(self):
        """

        每次取出slaver队列头部的一个, 测试心跳, 并把它放回尾部.
            slaver若超过 SPARE_SLAVER_TTL 秒未收到心跳, 则会自动重连
            所以睡眠间隔(delay)满足   delay * slaver总数  < TTL
            使得一轮循环的时间小于TTL,
            保证每个slaver都在过期前能被心跳保活
        """

        SPARE_SLAVER_TTL = 300
        default_delay = 5 + SPARE_SLAVER_TTL // 12
        delay = default_delay
        print("heart beat daemon start, delay: {}s".format(delay))
        while True:
            time.sleep(0.5)
            # ---------------------- preparation -----------------------
            slaver_count = len(self.__slaver_list)
            if not slaver_count:
                print("heart_beat_daemon: sorry, no slaver available, keep sleeping")
                # restore default delay if there is no slaver
                delay = default_delay
                continue
            else:
                # notice this `slaver_count*2 + 1`
                # slaver will expire and re-connect if didn't receive
                #   heartbeat pkg after SPARE_SLAVER_TTL seconds.
                # set delay to be short enough to let every slaver receive heartbeat
                #   before expire
                delay = 1 + SPARE_SLAVER_TTL // max(slaver_count * 2 + 1, 12)
            # pop the oldest slaver
            #   heartbeat it and then put it to the end of queue
            slaver = self.__slaver_list.pop(0)
            # ------------------ real heartbeat begin --------------------
            start_time = time.perf_counter()
            try:
                slaver.send(bytes("ACTIVATE", encoding="UTF-8"))
                PACKAGE_SIZE = 2 ** 6
                hb_result = self.__select_recv(slaver, PACKAGE_SIZE) == bytes("READY", encoding="UTF-8")
            except Exception as e:
                print("error during heartbeat: {}".format(e))
                hb_result = False
            finally:
                time_used = round((time.perf_counter() - start_time) * 1000.0, 2)
            # ------------------ real heartbeat end ----------------------
            if not hb_result:
                print("heart beat failed, time: {}ms".format(time_used))
                _helper.close_socket(slaver)
                del slaver

                # if heartbeat failed, start the next heartbeat immediately
                #   because in most cases, all 5 slaver connection will
                #   fall and re-connect in the same time
                delay = 0
            else:
                print("heartbeat success, time: {}ms".format(time_used))
                self.__slaver_list.append(slaver)

    @staticmethod
    def __select_recv(conn, buff_size, timeout=None):
        """add timeout for socket.recv()
        :type conn: socket.socket
        :type buff_size: int
        :type timeout: float
        :rtype: Union[bytes, None]
        """
        import selectors
        if selectors:
            sel = selectors.DefaultSelector()
            sel.register(conn, selectors.EVENT_READ)
            events = sel.select(timeout)
            sel.close()
            if not events:
                # timeout
                raise RuntimeError("recv timeout")
        else:
            rlist, _, _ = select.select([conn], [], [], timeout)
        buff = conn.recv(buff_size)
        if not buff:
            raise RuntimeError("received zero bytes, socket was closed")
        return buff

    def __pop_available_slaver(self) -> Optional[socket.socket]:
        r"""取出一个可用的slaver
        TODO: 方法加锁

        :return: 可用的slaver，无可用的slaver时返回None
        """

        while len(self.__slaver_list) > 0:
            slaver = self.__slaver_list.pop(0)
            try:
                # 发送握手消息校验："ACTIVATE"
                slaver.send(bytes("ACTIVATE", encoding="UTF-8"))
                # 校验客户端握手消息："READY"
                if slaver.recv(1024) == bytes("READY", encoding="UTF-8"):
                    # 握手校验通过，返回当前slaver
                    return slaver
            except (ConnectionAbortedError, ConnectionResetError):
                # 握手通讯校验失败，继续取列表中的下一个slaver
                continue
        # slaver列表耗尽，返回None
        return None


if __name__ == '__main__':
    # 解析命令行启动参数
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--master", required=True, metavar="host:port",
                        help="listening for slavers. eg: 0.0.0.0:8888")
    parser.add_argument("-c", "--customer", required=True, metavar="host:port",
                        help="listening for customers. eg: 127.0.0.1:3240")
    parser.add_argument("-D", "--debug", required=False, action="store_true", default=False,
                        help="print recv data (from slaver) and send data (from customer).")
    args = parser.parse_args()
    # 启动NATMaster
    Master(args.master, args.customer, args.debug)
    # 守护进程
    while True:
        time.sleep(1)
