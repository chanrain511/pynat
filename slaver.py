# -*- coding: utf-8 -*-

import select
import socket
import time

import _helper


class Slaver:

    def __init__(self, communicate_addr, target_addr, debug):
        r"""

        :param communicate_addr:
        :param target_addr:
        :param debug:
        """

        self.__alive_slaver_count = 0
        while True:
            if self.__alive_slaver_count < 5:
                try:
                    # 创建slaver客户端套接字
                    slaver_conn = _helper.create_sock_client(communicate_addr)
                    # 连接成功，进行初次握手校验
                    slaver_conn.send(bytes("HI", encoding="UTF-8"))
                    if slaver_conn.recv(1024) == bytes("OK", encoding="UTF-8"):
                        # 创建target客户端套接字
                        try:
                            target_conn = _helper.create_sock_client(target_addr)
                            # slaver发送就绪握手信息，即将开始交换数据
                            slaver_conn.send(bytes("READY", encoding="UTF-8"))
                            # 2次握手完毕，存活slaver计数+1
                            self.__alive_slaver_count += 1
                            print("新的slaver已连接，剩余：{}".format(self.__alive_slaver_count))
                            # 开启新线程交换数据
                            _helper.start_thread(self.__tcp_transfer, args=(slaver_conn, target_conn))
                        except (ConnectionRefusedError, TimeoutError):
                            print("连接target失败：{}".format(target_addr))
                    else:
                        # slaver初次握手校验失败
                        _helper.close_socket(slaver_conn)
                except (ConnectionRefusedError, TimeoutError):
                    print("连接communicate失败：{}".format(communicate_addr))
                except ConnectionResetError:
                    print("communicate主机强迫关闭了一个现有的连接：{}".format(communicate_addr))
            else:
                time.sleep(1)

    def __tcp_transfer(self, slaver_conn: socket.socket, target_conn: socket.socket):
        r"""

        :param slaver_conn:
        :param target_conn:
        :return:
        """

        while True:
            # 阻塞等待socket满足readable条件
            readable_sockets, _, _ = select.select([slaver_conn, target_conn], [], [])
            for item in readable_sockets:
                data = b""
                try:
                    if item == slaver_conn:
                        data = slaver_conn.recv(1024)
                        if data == bytes("ACTIVATE", encoding="UTF-8"):
                            # 处理服务端ACTIVATE握手
                            slaver_conn.send(bytes("READY", encoding="UTF-8"))
                        else:
                            # 正常交换数据
                            target_conn.send(data)
                    elif item == target_conn:
                        data = target_conn.recv(1024)
                        slaver_conn.send(data)
                except ConnectionResetError:
                    print("远程主机强迫关闭了一个现有的连接")
                    _helper.close_socket(slaver_conn)
                    _helper.close_socket(target_conn)
                    # 存活slaver计数-1
                    self.__alive_slaver_count -= 1
                    print("slaver已关闭，剩余：{}".format(self.__alive_slaver_count))
                    return
                except Exception as e:
                    # TODO: 确认可能抛出的异常类型
                    print(e)
                    _helper.close_socket(slaver_conn)
                    _helper.close_socket(target_conn)
                    # 存活slaver计数-1
                    self.__alive_slaver_count -= 1
                    print("slaver已关闭，剩余：{}".format(self.__alive_slaver_count))
                    return
                # 没有数据，结束线程
                # TODO: 异常处理
                if not data:
                    _helper.close_socket(slaver_conn)
                    _helper.close_socket(target_conn)
                    # 存活slaver计数-1
                    self.__alive_slaver_count -= 1
                    print("slaver已关闭，剩余：{}".format(self.__alive_slaver_count))
                    return


if __name__ == '__main__':
    # 解析命令行启动参数
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--master", required=True, metavar="host:port",
                        help="connect for slavers. eg: 192.168.5.10:8888")
    parser.add_argument("-t", "--target", required=True, metavar="host:port",
                        help="listening for target. eg: 127.0.0.1:3240")
    parser.add_argument("-D", "--debug", required=False, action="store_true", default=False,
                        help="print recv data (from slaver) and send data (from customer).")
    args = parser.parse_args()
    # 启动NATClient
    Slaver(args.master, args.target, args.debug)
