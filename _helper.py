# -*- coding: utf-8 -*-

import socket
import threading


def create_sock_server(bind_addr: str) -> socket.socket:
    r"""创建用于端口转发的socket服务端

    :param bind_addr: socket绑定的完整IP地址（包含端口号）
    :return: socket对象
    """

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind((addr_ip(bind_addr), addr_port(bind_addr)))
    sock.listen(5)
    sock.setblocking(True)
    return sock


def create_sock_client(conn_addr: str) -> socket.socket:
    r"""创建用于端口转发的socket客户端

    :param conn_addr: socket连接的完整IP地址（包含端口号）
    :return: socket对象
    """

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    sock.connect(fmt_addr(conn_addr))
    return sock


def addr_ip(addr: str) -> str:
    r"""获取完整IP地址的IP部分（不包含端口号）

    :param addr: 完整IP地址
    :return: IP地址IP部分
    """

    return addr.split(":")[0]


def addr_port(addr: str) -> int:
    r"""获取完整IP地址的端口号部分（不包含IP）

    :param addr: 完整IP地址
    :return: 的IP地址的端口号部分
    """

    return int(addr.split(":")[1])


def fmt_addr(addr: str):
    r"""格式化IP地址：0.0.0.0:8000 -> ("0.0.0.0", 8000)

    :param addr:
    :return:
    """

    return addr_ip(addr), addr_port(addr)


def start_thread(target, args: tuple = ()):
    r"""启动新的线程

    :param target: 线程执行的方法f()
    :param args: 线程方法参数元组(arg1,)或(arg1, arg2, ...)
    :return:
    """

    thread = threading.Thread(target=target, args=args)
    thread.setDaemon(True)
    thread.start()


def close_socket(socket_conn: socket.socket):
    r"""关闭套接字连接

    :param socket_conn: 要关闭的套接字连接
    :return:
    """

    try:
        socket_conn.close()
    except Exception as e:
        print(e)
